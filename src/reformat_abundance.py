#!/usr/bin/python
# melt, reshape data to desired format for a private project, map attributes
import argparse
import datetime
import time
import os
import numpy as np
import pandas as pd

def reformat_cols(data: pd.DataFrame, var_attributes: pd.DataFrame,
                  additional_id: pd.DataFrame, const_attributes:
                  dict={"Type_of_Experiment": "RNA-Seq",
                  "Units": "log2_cpm", "Imputed": "FALSE"},
                  col_names: list=["entity_id", "additional_id",
                  "replicate_name", "Log_Counts"], col_order: list=["Species",
                  "Strain", "Type_of_Experiment", "entity_id", "Treatment_Type",
                  "replicate_name", "Units", "Log_Counts", "Imputed",
                  "additional_id"]) -> pd.DataFrame:
    """
    Example of one full entry (single line tsv):

        Species             Foo
        Strain              Bar
        Type_of_Experiment  RNA-Seq
        entity_id           GeneID
        Treatment_Type      Treatment_1
        replicate_name      Sample_ID
        Units               log2_cpm
        Log_Counts          3.142
        Imputed             FALSE
        additional_id       Alternate_GeneID
    """
    data.rename(columns=dict(zip(data.columns, col_names)), inplace=True)
    for i, j in const_attributes.items():
        data[i] = j
    data["replicate_name"] = data.replicate_name.astype(np.int64)
    data = data.merge(var_attributes, left_on="replicate_name",
                      right_on="replicate_name", how="left")
    data.drop(columns=["additional_id"], inplace=True)
    data = data.merge(additional_id, left_on="entity_id", right_on="entity_id")
    data = data.reindex(columns=col_order)
    data.sort_values(by=["replicate_name", "entity_id"], inplace=True)
    return data

def parse_mappings_sample(infile_path: str, col_names: list=["Species", "Strain",
                   "Treatment_Type", "replicate_name"]) -> pd.DataFrame:
    """Load sample mappings for reference and later merging

        gzip -cd multi_omics_master_heatmap_table.tsv.gz | \
            cut -f4,10 | sort | uniq | gzip > mapfile_gene.tsv.gz
    """
    data = pd.read_csv(infile_path, sep="\t", header=None)
    data.rename(columns=dict(zip(data.columns, col_names)), inplace=True)
    return data

def parse_mappings_gene(infile_path: str, col_names: list=["entity_id",
                        "additional_id"]) -> pd.DataFrame:
    """Load gene mappings (manually parsed gff files)

        # glue together all relevant gff files
        cat $(find . -name "*gff" -type f 2>/dev/null | \
            grep "new" | grep "annot" | grep "GC" | grep -v "16052019") > \
            all.gff

        # get the attribute field specifically
        grep -vP "^#" all.gff | cut -f9 | grep "locus_tag" > id.gff

        # each gff file is formatted inconsistently
        grep -P "locus_tag.*protein_id" id.gff | \
            perl -pe "s|.*locus_tag=(.*?);.*protein_id=(.*?);.*|\1\t\2|" > \
            locus_tag_protein_id.map
    """
    data = pd.read_csv(infile_path, sep="\t", header=None)
    data.rename(columns=dict(zip(data.columns, col_names)), inplace=True)
    return data

def main():
    parser = argparse.ArgumentParser(description=
        "Reformat file(s) of abundance measurements into a desired format."
        )
    parser.add_argument("input_data", type=str, nargs="+",
                        help="Provide paths to data. If multiple \
                        files provided, will read from all files.")
    parser.add_argument("-s", "--mapfile_sample", type=str,
                        help="sample mapping file")
    parser.add_argument("-g", "--mapfile_gene", type=str,
                        help="gene mapping file")
    parser.add_argument("-o", "--outfile_dir", type=str,
                        default="./", help="Provide path to output dir.")
    args = parser.parse_args()

    gene_map = parse_mappings_gene(args.mapfile_gene)
    var_attributes = parse_mappings_sample(args.mapfile_sample)

    for infile_path in args.input_data:
        data = pd.read_csv(infile_path, sep="\t")
        data = data.melt(id_vars=["GeneID", "GeneName"])
        data = reformat_cols(data, var_attributes, gene_map)
        suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S%f")
        time.sleep(0.01)
        outfile_path = "".join([args.outfile_dir, "/",
                                os.path.split(infile_path)[-1], ".out.", suffix]
                               )
        print(" ".join(["Writing file to", outfile_path]))
        data.to_csv(outfile_path, sep="\t", index=False)

if __name__ == "__main__":
    main()
