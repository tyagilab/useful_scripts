# README

## Authors

- Shaun Dabare
- [Sonika Tyagi](https://orcid.org/0000-0003-0181-6258)
- [Tyrone Chen](https://orcid.org/0000-0002-9207-0385)

## Usage

Using the small example data in the repository, this writes the file `test.xmfa.fasta` to `../data`. You can modify the output file path and name by adding the `-o` option.

```
python parse_mauve.py ../data/test.xmfa -f 21
```

The `-f` option makes us keep all ungapped seqs above this length. 0 to keep all.

```
python parse_mauve.py ../data/test.xmfa -f 21 -o ./test.fasta
```

The `-n` option makes us discard seqs with an `N` proportion above this quantity. 1.0 to keep all, 0 to discard anything with `N`.

```
python parse_mauve.py ../data/test.xmfa -f 21 -o ./test.fasta
```

Progress bar is enabled by default. You can disable this in a non-interactive environment (eg HPC) by adding `--disable_progress_bar`

```
python parse_mauve.py ../data/test.xmfa -f 21 -o ./test.fasta --disable_progress_bar
```

## Dependencies

```
python=3.7.3
biopython=1.7.3
tqdm=4.46.1
```
