#!/usr/bin/python
# take a mauve multiseq alignment, remove gaps & N and split to multifasta file.
import argparse
import os
import re
import warnings
from Bio import SeqIO
from tqdm import tqdm

def parse_msa(seq: str, outfile_path: str=None, header: str=None,
              filter_len: int=0, offset: int=0, remove_regex: str="[^ACGTN\-]"):
    """
    Parse a string of nucleotides and filter out those meeting criteria.

    Arguments:
        (REQUIRED) seq - a string of nucleotides. gaps, N will be removed.
        (OPTIONAL) outfile_path - write output fasta file to this location.
        (OPTIONAL) header - prepend original header to output fasta header.
        (OPTIONAL) filter_len - filter sequence length below this size.
        (OPTIONAL) offset - offset coordinates in output if not 0 start.
        (OPTIONAL) remove_regex - remove all values in this regex pattern

    Input:
        >header 2:66
        CTGTTTGTTGAATTGACTAGATN----CCAGA---------GACTAGATGATCTCAAAGGCAAA

    Output:
        >header 2:24
        CTGTTTGTTGAATTGACTAGAT
        >header 43:66
        GACTAGATGATCTCAAAGGCAAA
    """
    # expect only nucleotides, gaps and N by default
    seq = re.sub(remove_regex, "", seq)

    # for the purposes of this script, we treat N the same as a gap
    seq = re.sub("N", "-", seq)
    indices = _parse_indices(seq, regex=r'-+')

    # fasta reads must start with ">"
    if not header.startswith(">"):
        header = "".join([">", header])

    for x, y in indices:
        # we want seqs which are greater than N nucleotides long
        if ((y - x) > filter_len):
            # ((float(seq.count("N")) / float(len(seq))) * 100 <= n_prop):
            filtered = "".join(
                [header, "_", str(x+offset), ":", str(y+offset), "\n", seq[x:y]]
                )
            if not outfile_path is None:
                with open(outfile_path, mode="a+") as outfile:
                    outfile.write("{}\n".format(filtered))

def _parse_indices(seq, regex):
    # get indices where all gaps start and end
    indices = [(m.start(), m.end()) for m in re.finditer(regex, seq)]
    # from this, get indices where seqs start and end
    indices_flat = [0] + [x for y in indices for x in y] + [len(seq)]
    # transform to list of str indices
    indices = iter(indices_flat)
    return zip(indices, indices)

def _argument_parser():
    parser = argparse.ArgumentParser(description=
        "Load mauve multiple sequence alignment, ungap and split to fasta file \
        Usage: python parse_msa.py [/path/to/data/*.xmfa] \
            -o [/path/to/out/*.fasta]"
        )
    parser.add_argument("infile_path", type=str,#type=argparse.FileType("r"),
                        help="Provide path to multiple sequence alignment file.")
    parser.add_argument("-o", "--outfile_path", type=str,
                        help="Provide path to output dir for fasta files.")
    parser.add_argument("-f", "--filter_len", type=int, default=0,
                        help="Keep seqs longer than this. (DEFAULT: 0)")
    parser.add_argument("-r", "--remove_regex", type=int, default=0,
                        help="Remove anything matching this regex pattern. \
                        (by default remove everything that is NOT ACGT-N) \
                        (DEFAULT: [^ACGTN\-])")
    parser.add_argument("-p", "--disable_progress_bar", action="store_true",
                        help="If True, do not show progress bar (useful for \
                        job logging on remote clusters) (DEFAULT: FALSE)")
    return parser.parse_args()

def main():
    args = _argument_parser()
    infile_path = args.infile_path
    outfile_path = args.outfile_path
    filter_len = args.filter_len

    if outfile_path is None:
        outfile_path = ".".join([infile_path, "fasta"])
        print("No output file specified, writing to input dir:", outfile_path)

    if os.path.exists(outfile_path):
        warnings.warn("Output file exists, overwriting!", category=UserWarning)
        os.remove(outfile_path)

    # for the progress bar, not used otherwise.
    with open(infile_path, mode='r') as handle:
        readcount = len([record.id for record in SeqIO.parse(handle, "fasta")])

    with open(infile_path, mode='r') as handle:
        for record in tqdm(SeqIO.parse(handle, 'fasta'), total=readcount,
                           desc="Degapping and filtering sequences...",
                           disable=args.disable_progress_bar):
        # for record in SeqIO.parse(handle, 'fasta'):
            header = record.description
            # we are assuming here quite a specific header format, eg:
            #   > 2:79430486-79505298 + Tasmanian-Devil-chrX.fna
            offset = int(header.split(":")[1].split("-")[0])
            parse_msa(seq=str(record.seq),
                      outfile_path=outfile_path,
                      header=header,
                      filter_len=filter_len,
                      offset=offset)

if __name__ == "__main__":
    main()
