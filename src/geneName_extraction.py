import re
def get_gene_name(gene_id_given, filename):
    
    with open(filename, 'r') as fp:
        lines = fp.readlines()

        for ix, line in enumerate(lines):
            line_elements = line.split('\t')
            if len(line_elements) > 1 and line_elements[2] == 'gene':
                gene_id = re.search(r'gene_id "(.*?)"', line_elements[8]).group(1)
#                 print(gene_id, gene_id_given)
                if gene_id_given == gene_id:
                    gene_name = re.search(r'gene_name "(.*?)"', line_elements[8]).group(1)
                    return gene_name
                
get_gene_name('ENSG00000241599.1', '../data/gencode.v34.long_noncoding_RNAs.gtf')