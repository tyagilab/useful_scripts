# README

## Authors

[Tyrone Chen](https://orcid.org/0000-0002-9207-0385)

This converts a `.tsv` file(s) to a flat format and adds mappings. You can modify the output file directory by adding the `-o` option. `.gz` files are also accepted. Sample test data not included as it is private at time of writing. The below sample files provide some example templates.

```
python reformat_sepsis.py test1.tsv test2.tsv -s mappings.tsv -g locus_tag_protein_id.tsv -o ./
```

Sample input file format:

```
test1.tsv

GeneID  GeneName  Sample_A  Sample_B  ...
gene1   gene1name 3.142     1.412     ...
...
```

Sample mappings (sample) format:

```
Species Strain  Treatment_1 Sample_A
Species Strain  Treatment_1 Sample_B
Species Strain  Treatment_2 Sample_C
Species Strain  Treatment_2 Sample_D
...
```

Sample mappings (gene) format:

```
FOO   BAR
SPAM  EGGS
...
```

## Dependencies

```
python=3.8.5
pandas=1.1.0
numpy=1.19.1
```
