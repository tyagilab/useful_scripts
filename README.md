# Useful scripts

Contains various useful scripts for bioinformatics. 

(Add yourself to the copyright notice and contributors list).

Contributors:

- [Sonika Tyagi](https://orcid.org/0000-0003-0181-6258)
- [Tyrone Chen](https://orcid.org/0000-0002-9207-0385)